CREATE OR REPLACE PACKAGE book_store AS 
    FUNCTION get_price_after_tax(book_isbn IN VARCHAR2)
        RETURN NUMBER;
    TYPE Customer_id_array IS VARRAY(100) OF NUMBER;
    FUNCTION book_purchasers(book_isbn IN VARCHAR2)
        RETURN Customer_id_array;
    PROCEDURE show_purchases;
END book_store;
/
CREATE OR REPLACE PACKAGE BODY book_store AS
 --internal function
 FUNCTION price_after_discount(book_isbn IN VARCHAR2)
        RETURN NUMBER IS
        discounted_price NUMBER;
        retail_price NUMBER;
        discount NUMBER;
    BEGIN
        --check for the retail price
        SELECT b.retail INTO retail_price 
        FROM books b
        WHERE b.isbn = book_isbn;
        --check for discount
        SELECT b.discount INTO discount
        FROM books b
        WHERE b.isbn = book_isbn;
        --add the discount
        discounted_price := retail_price-discount;
        RETURN discounted_price;
    END;
--public function
FUNCTION get_price_after_tax(book_isbn IN VARCHAR2)
        RETURN NUMBER IS
        final_price NUMBER;
        new_price NUMBER;
        tax NUMBER;
    BEGIN
        --discount price + tax
        new_price := price_after_discount(book_isbn);
        tax := 1.15;
        final_price := new_price*tax;
        RETURN final_price;
    END;
--Added function for assignment 3
FUNCTION book_purchasers(book_isbn IN VARCHAR2)
        RETURN Customer_id_array IS
        customers_id Customer_id_array;
    BEGIN
        SELECT c.customer# BULK COLLECT INTO customers_id
        FROM customers c
        JOIN orders o
        ON c.customer# = o.customer#
        JOIN orderitems oi
        ON o.order# = oi.order#
        JOIN books b
        ON oi.isbn = b.isbn
        WHERE b.isbn = book_isbn;
        RETURN(customers_id);
    END;
--Procedure to call show the book purchasers for Assignment 3
    PROCEDURE show_purchases AS
        purchasers_id Customer_id_array;
        customers_full_name VARCHAR2(30);
    BEGIN
        FOR book_cursor IN (SELECT * FROM books)LOOP
            DBMS_OUTPUT.PUT(book_cursor.isbn || ': ' || book_cursor.title || ': ' );
            purchasers_id := book_purchasers(book_cursor.isbn);
            FOR i in 1 .. purchasers_id.COUNT LOOP
                SELECT (c.firstname || ' ' || c.lastname) INTO customers_full_name
                FROM customers c
                WHERE c.customer# = purchasers_id(i);
                DBMS_OUTPUT.PUT(customers_full_name || ', ');
            END LOOP;
            DBMS_OUTPUT.PUT_LINE('');
            DBMS_OUTPUT.PUT_LINE('');
        END LOOP;
    END;
END book_store;
/
--anonymous block 
BEGIN
    book_store.show_purchases;
END;