CREATE OR REPLACE PACKAGE book_store AS 
    FUNCTION get_price_after_tax(book_isbn IN VARCHAR2)
        RETURN NUMBER;
END book_store;
/
CREATE OR REPLACE PACKAGE BODY book_store AS
 --internal function
 FUNCTION price_after_discount(book_isbn IN VARCHAR2)
        RETURN NUMBER IS
        discounted_price NUMBER;
        retail_price NUMBER;
        discount NUMBER;
    BEGIN
        --check for the retail price
        SELECT b.retail INTO retail_price 
        FROM books b
        WHERE b.isbn = book_isbn;
        --check for discount
        SELECT b.discount INTO discount
        FROM books b
        WHERE b.isbn = book_isbn;
        --add the discount
        discounted_price := retail_price-discount;
        RETURN discounted_price;
    END;
--public function
FUNCTION get_price_after_tax(book_isbn IN VARCHAR2)
        RETURN NUMBER IS
        final_price NUMBER;
        new_price NUMBER;
        tax NUMBER;
    BEGIN
        --discount price + tax
        new_price := price_after_discount(book_isbn);
        tax := 1.15;
        final_price := new_price*tax;
        RETURN final_price;
    END;
END book_store;
/
DECLARE
finalbook1 NUMBER(5,2);
finalbook2 NUMBER(5,2);
BEGIN
finalbook1 := book_store.get_price_after_tax('4981341710'); 
finalbook2 := book_store.get_price_after_tax('3957136468');
DBMS_OUTPUT.PUT_LINE(finalbook1);
DBMS_OUTPUT.PUT_LINE(finalbook2);
END;