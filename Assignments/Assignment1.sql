--drop area
DROP TABLE Person;
DROP TABLE Home;
DROP TABLE Occupation;

--create area
CREATE TABLE Occupation
(
    oid         char(4)         PRIMARY KEY,
    type        varchar(20),
    salary      number(10,2)
);

CREATE TABLE Home
(
    hid         char(4)         PRIMARY KEY,
    address     varchar(200)
);

CREATE TABLE Person
(
    pid         char(4)         PRIMARY KEY,
    firstname   varchar(20)     NOT NULL,
    lastname    varchar(20)     NOT NULL,
    mother_id   char(4)         REFERENCES Person(pid),
    father_id   char(4)         REFERENCES Person(pid),
    hid         char(4)         REFERENCES Home(hid),
    oid         char(4)         REFERENCES Occupation(oid)
);

--inserting values
INSERT INTO Home
    VALUES ('H001', '123 Easy St.');
INSERT INTO Home
    VALUES ('H002', '56 Fake Ln.');

INSERT INTO Occupation
    VALUES ('O000', 'N/A', 0);
INSERT INTO Occupation
    VALUES ('O001', 'Student', 0);
INSERT INTO Occupation
    VALUES ('O002', 'Doctor', 100000);
INSERT INTO Occupation
    VALUES ('O003', 'Professor', 80000);
    
INSERT INTO Person(pid, firstname, lastname, hid, oid)
    VALUES ('P001', 'Zachary', 'Aberny', 'H002', 'O000');
INSERT INTO Person(pid, firstname, lastname, hid, oid)
    VALUES ('P002', 'Yanni', 'Aberny', 'H002', 'O000');
INSERT INTO Person
    VALUES ('P003', 'Alice', 'Aberny', 'P002', 'P001', 'H001', 'O002');
INSERT INTO Person(pid, firstname, lastname, hid, oid)
    VALUES ('P004', 'Bob', 'Bortelson', 'H001', 'O003');
INSERT INTO Person
    VALUES ('P005', 'Carl', 'Aberny-Bortelson', 'P003', 'P004', 'H001', 'O001');
INSERT INTO Person
    VALUES ('P006', 'Denise', 'Aberny-Bortelson', 'P003', 'P004', 'H001', 'O001');

--testing
SELECT gp.firstname
FROM person gp
JOIN person p ON gp.pid = p.mother_id OR gp.pid = p.father_id
JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id
WHERE c.firstname = 'Denise';

--question 1
SELECT h.address, COUNT(p.pid) AS "AMOUNT OF HABITANTS"
FROM home h
JOIN person p ON h.hid = p.hid
GROUP BY h.hid, h.address;

--question 2
SELECT DISTINCT gp.firstname, gp.lastname
FROM person gp
JOIN person p ON gp.pid = p.father_id
JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id;

--question 3
SELECT DISTINCT s.firstname, s.lastname
FROM person s
JOIN person p ON s.father_id = p.pid OR s.mother_id = p.pid
WHERE s.oid = 'O001' AND s.hid = p.hid;

--question 4
SELECT MAX(SUM(o.salary)) AS "HIGHEST HOUSEHOLD INCOME"
FROM person p
JOIN occupation o ON p.oid = o.oid
JOIN home h ON h.hid = p.hid
GROUP BY h.hid, h.address;